import urllib.parse


def main():
    # csv_path = '/var/www/dorsbackend/ru-clip.net.organic.keys.gru.csv'
    csv_path = '/Users/kirill/Desktop/keys.txt'

    urls = []
    for line in open(csv_path, 'r', encoding="utf-8"):
        keyword = line.strip()
        url = f'https://ru-clip.net/rev/{urllib.parse.quote_plus(keyword)}'
        urls.append(url)

    values = ""
    for item in urls:
        values += f"('{item}', false, null),\n"

    values = values[:-2]
    query = f"""
    INSERT INTO ruclip_parsed.public.urls (url, is_crawled, crawl_date)
    VALUES {values}
    ON CONFLICT (url) DO NOTHING;
    """

    with open('res.sql', 'w') as f:
        f.write(query)


if __name__ == '__main__':
    main()
