import pytest

from parsers.utils import slugify


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ('Форт Боярд | Выпуск 4', 'форт-боярд-выпуск-4'),
        ('🔴PREDICCIÓN 20 de Julio 2019 que sucederá❓', 'predicción-20-de-julio-2019-que-sucederá'),
        ('МОЩНЫЙ ПРОМО-КОД 1XBET. Бонусы 1xbet. 5000 рублей на баланс.',
         'мощный-промо-код-1xbet-бонусы-1xbet-5000-рублей-на-баланс'),
    ])
def test_slugify(test_input, expected):
    slug = slugify(test_input)
    assert slug == expected
