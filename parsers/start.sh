#!/bin/bash

cd /var/www/ruclip_parser/
. venv/bin/activate
cd app/app/spiders
scrapy crawl ruclip --logfile=ruclip_logs.log
