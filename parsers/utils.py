import re


def slugify(url: str) -> str or None:
    try:
        s = url
        s = re.sub('[^\\w\\s-]', '', s).strip().lower()
        s = " ".join(re.split("\\s+", s, flags=re.UNICODE))
        s = s.replace(' ', '-')
        s = re.sub(r'-+', '-', s)
        return s
    except TypeError:
        return None


def get_ruclip_video_item(response) -> dict:
    try:
        video = dict(
            title=response.css('strong.title>a::text').get(),
            channel=response.css('div.vid-info>.clearfix>a::text').get(),
            channel_id=str(response.css('div.vid-info>.clearfix>a::attr(href)').get()).split('/')[-1],
            video_id=response.css('a.video_link::attr(href)').get().split('/')[2],
        )
        if all(video) is None:
            raise TypeError('Не удалось спарсить ни один из элементов видео')
        views = response.css('div.view-date>span::text').get()
        if views:
            video.update(dict(views=views))

        short_description = response.css('div.sdescr::text').get()
        if short_description:
            video.update(dict(short_description=short_description))
        return video
    except TypeError:
        raise TypeError


def _check_all_slug_are_valid():
    from db.models import Video
    new_videos = []
    for video in Video.select():
        if not video.title:
            video.delete_by_id(video.id)
            print('deleted ', video.id)
        valid_slug = slugify(video.title)
        if video.slug != valid_slug:
            print('video.slug != valid_slug   |    ', video.id)

    if new_videos:
        Video.bulk_update(new_videos, fields=[Video.slug])


def run_trends_parser():
    from scrapy.crawler import CrawlerProcess
    from parsers.app.spiders.trends_parser import RuClipTrendsParser
    process = CrawlerProcess()
    process.crawl(RuClipTrendsParser)
    process.start()


if __name__ == '__main__':
    _check_all_slug_are_valid()
