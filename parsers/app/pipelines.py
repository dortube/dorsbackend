# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from db import models
from parsers.utils import slugify


class SaveToPostgresPipeline(object):
    def __init__(self):
        self.connection = None

    def open_spider(self, spider):
        self.connection = models.db.connect(reuse_if_open=True)

    def close_spider(self, spider):
        models.db.close()

    def process_item(self, item, spider):
        """ Сохранить данные в базу. Вызывается для каждого элемента в пайплайне. """
        if str(item) == 'VideoItem':
            self.process_video_item(item, spider.logger)
        elif str(item) == 'SearchPageItem':
            self.process_searchpage_item(item, spider.logger)
        elif str(item) == 'ChannelItem':
            self.process_channel_item(item, spider.logger)
        elif str(item) == 'TrendingPageItem':
            self.process_trendingpage_item(item, spider.logger)
        elif str(item) == 'LivesPageItem':
            self.process_livespage_item(item, spider.logger)
        return item

    @staticmethod
    def process_trendingpage_item(item, logger):
        # если есть, удаляем старые видео
        query = models.Trending.delete().where(models.Trending.country == item['country'])
        query.execute()

        # добавляем новые видео
        models.Trending.create(country=item['country'], videos=item['videos'])
        logger.info(f"TrendingPageItem {item['country']} успешно добавлен в базу")

    @staticmethod
    def process_livespage_item(item, logger):
        # если есть, удаляем старые видео
        models.Lives.delete().execute()

        # добавляем новые видео
        obj = models.Lives.create(videos=item['videos'])
        logger.info(f"LivesPageItem успешно добавлен в базу {obj.last_update}")

    @staticmethod
    def process_searchpage_item(item, logger):
        keyword_exist = models.SearchPage.get_or_none(slug=slugify(item['keyword']))
        if keyword_exist:
            logger.info(f"SearchPageItem {item['keyword']} уже существует в базе")
        else:
            models.SearchPage.create(
                keyword=item['keyword'], videos=item['videos'], slug=slugify(item['keyword'])
            )
            logger.info(f"SearchPageItem {item['keyword']} успешно добавлен в базу")

    @staticmethod
    def process_channel_item(item, logger):
        # проверить есть ли уже такой канал в базе
        channel_obj = models.Channel.get_or_none(channel_id=item['channel_id'])
        if not channel_obj:
            logger.info(f"Канала {item['channel_id']} еще нет в базе. Добавляем, но только с дефолтными полями")
            new_obj = models.Channel(
                name=item['name'],
                channel_id=item['channel_id'],
                description=None,
                background_url=None,
                avatar_url=None,
            )
            new_obj.save()
            return

        # если канал уже доступен, проверить есть ли в нем поля, кроме дефолтных
        if not channel_obj.description:
            logger.info(f"У канала {channel_obj.channel_id} только дефолтные поля. Добавляем остальные")
            channel_obj.description = item['description']
            channel_obj.background_url = item['background_url']
            channel_obj.avatar_url = item['avatar_url']
            channel_obj.save()
            logger.info(f"Канал {channel_obj.channel_id} был полностью загружен в базу")

    @staticmethod
    def process_video_item(item, logger):
        """ Обработка видео айтема (страница с видео) """

        # прежде чем что-то обрабатывать и добавлять новое видео, нужно чекнуть что оно уже не добавлено ранее
        video_obj = models.Video.get_or_none(video_id=item['video_id'])
        if video_obj is not None:
            logger.info(f"Видео {video_obj.video_id} уже добавлено в базу")
            return

        # ищем и если отсутствует - то создаем новый обьект канала
        # к сожалению, на данном этапе о канале известно только его имя и id,
        # поэтому остальную инфу придется парсить отдельно.
        channel_obj = models.Channel.get_or_none(channel_id=item['channel_id'])
        if not channel_obj:
            channel_obj = models.Channel(
                name=item['channel_name'],
                channel_id=item['channel_id'],
                description=None,
                background_url=None,
                avatar_url=None,
            )
            channel_obj.save()
            logger.info(f"Канал {channel_obj.channel_id} успешно добавлен в базу с дефолтными полями")

        # формируем словарь с лайками
        likes = {'likes': item['likes'], 'dislikes': item['dislikes']}

        category_obj = None
        if item['category']:
            category_obj, created = models.Category.get_or_create(
                name=item['category'],
                slug=slugify(item['category'])
            )
            item['category'] = category_obj

        slug = slugify(item['title'])

        logger.debug(f"Подготовлены поля к сохранению: {item.keys()}")

        # наконец создаем новый обьект
        models.Video.create(
            video_id=item['video_id'],
            slug=slug,
            views=item['views'],
            title=item['title'],
            description=item['description'],
            likes=likes,
            upload_date=item['upload_date'],
            channel=channel_obj,
            category=category_obj
        )
        logger.info(f"Видео {item['video_id']} успешно добавлено в базу")
