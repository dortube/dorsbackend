import datetime
import locale
import urllib.parse
from urllib.parse import unquote

import scrapy
from scrapy import signals

from db.models import URLS
from parsers.app.items import SearchPageItem
from parsers.utils import slugify, get_ruclip_video_item


class RuClipSpageParser(scrapy.Spider):
    name = "spage"
    allowed_domains = ["ru-clip.net"]

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(RuClipSpageParser, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.item_scraped, signal=signals.item_scraped)
        return spider

    @staticmethod
    def item_scraped(item, response, spider):
        if str(item) == 'SearchPageItem':
            spider.logger.info('SearchPageItem сохранен')

            # response_url = str(response.url).replace('http', 'https')
            url_obj = URLS.get_or_none(URLS.url == response.url)
            if not url_obj:
                encoded_url = unquote(response.url)
                url_obj = URLS.get_or_none(URLS.url == encoded_url)
                if not url_obj:
                    spider.logger.warning(f'Получил None при запросе урла {response.url} из базы')
                    return
            url_obj.is_crawled = True
            url_obj.crawl_date = datetime.datetime.utcnow()
            url_obj.save()
            spider.logger.info(f'Урл {response.url} помечен пройденным')

    def start_requests(self):
        locale.setlocale(locale.LC_ALL, ('RU', 'UTF8'))
        urls = URLS.select().order_by(URLS.id.desc()).where(URLS.is_crawled == False)[:100000]  # noqa
        for url in urls:
            if '/rev/' in url.url:
                u = 'http://ru-clip.net/rev/{}'.format(urllib.parse.quote(str(url.url).split('/rev/')[1]))
                yield scrapy.Request(url=u, callback=self.parse)

    def parse(self, response):
        """ Парсинг страницы результатов поиска + отедьно видео, которые есть на странице. """
        self.logger.info(f'Начинаем парсинг страницы {response.url}')

        keyword = response.xpath('//div/div/header/h1/text()').get()
        self.logger.info(f"Парсим страницу результатов поиска по запросу '{keyword}'")
        videos = []
        for video in response.css('div.videos-block'):
            try:
                upload_date = video.css('div.view-date>span::text')[1].get()
            except IndexError:
                upload_date = None

            slug = slugify(video.css('strong.title>a::text').get())
            try:
                video = get_ruclip_video_item(video)
                video.update({'slug': slug, 'upload_date': upload_date})
                videos.append(video)
            except (IndexError, TypeError, AttributeError) as e:
                self.logger.debug(e)
                continue
        search_page_item = SearchPageItem(keyword=keyword, videos=videos)
        self.logger.info(f"Спарсили страницу результатов поиска по запросу '{keyword}'")
        yield search_page_item
