import locale

import scrapy

from parsers.app.items import LivesPageItem
from parsers.utils import slugify, get_ruclip_video_item


# todo: парсить страницу видео

class RuClipLivesParser(scrapy.Spider):
    name = "lives"
    allowed_domains = ["ru-clip.net"]

    def start_requests(self):
        locale.setlocale(locale.LC_ALL, ('RU', 'UTF8'))
        url = 'https://ru-clip.net/livetv'
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        """ Парсинг страницу с прямыми эфирами """
        self.logger.info(f'Начинаем парсинг страница с прямыми эфирами')

        videos = []
        for video in response.css('div.video-block'):
            try:
                upload_date = video.css('div.vid-info>div.clearfix>span::text').get()
            except IndexError:
                upload_date = None

            slug = slugify(video.css('strong.title>a::text').get())

            try:
                video = get_ruclip_video_item(video)
                video.update({'slug': slug, 'upload_date': upload_date})
                videos.append(video)
            except IndexError:
                continue
        lives_item = LivesPageItem(videos=videos)
        self.logger.info(f"Спарсили страницу прямых трансляций")
        yield lives_item
