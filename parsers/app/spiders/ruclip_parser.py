import datetime
import locale
import urllib.parse
from urllib.parse import unquote

import scrapy
from scrapy import signals

from db.models import URLS
from parsers.app.items import SearchPageItem, VideoItem, ChannelItem
from parsers.utils import slugify, get_ruclip_video_item


class RuClipParser(scrapy.Spider):
    name = "ruclip"
    allowed_domains = ["ru-clip.net"]

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(RuClipParser, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.item_scraped, signal=signals.item_scraped)
        return spider

    @staticmethod
    def item_scraped(item, response, spider):
        if str(item) == 'SearchPageItem':
            spider.logger.info('SearchPageItem сохранен')

            # response_url = str(response.url).replace('http', 'https')
            url_obj = URLS.get_or_none(URLS.url == response.url)
            if not url_obj:
                encoded_url = unquote(response.url)
                url_obj = URLS.get_or_none(URLS.url == encoded_url)
                if not url_obj:
                    spider.logger.warning(f'Получил None при запросе урла {response.url} из базы')
                    return
            url_obj.is_crawled = True
            url_obj.crawl_date = datetime.datetime.utcnow()
            url_obj.save()
            spider.logger.info(f'Урл {response.url} помечен пройденным')

    def start_requests(self):
        locale.setlocale(locale.LC_ALL, ('RU', 'UTF8'))
        urls = URLS.select().where(URLS.is_crawled == False)[:500]  # noqa
        for url in urls:
            if '/rev/' in url.url:
                u = 'http://ru-clip.net/rev/{}'.format(urllib.parse.quote(str(url.url).split('/rev/')[1]))
                yield scrapy.Request(url=u, callback=self.parse)

    def parse(self, response):
        """
            Парсинг страницы результатов поиска + отедьно видео, которые есть на странице.
            Порядок важен: сначала парсим все видео настранице, потом страницу в целом.
        """
        self.logger.info(f'Начинаем парсинг страницы {response.url}')
        for href in response.css('a.video_link::attr(href)'):
            yield response.follow(href, self.parse_video)

        keyword = response.xpath('//div/div/header/h1/text()').get()
        self.logger.info(f"Парсим страницу результатов поиска по запросу '{keyword}'")
        videos = []
        for video in response.css('div.videos-block'):
            try:
                upload_date = video.css('div.view-date>span::text')[1].get()
            except IndexError:
                upload_date = None

            slug = slugify(video.css('strong.title>a::text').get())
            video = get_ruclip_video_item(video)
            video.update({'slug': slug, 'upload_date': upload_date})
            videos.append(video)
        search_page_item = SearchPageItem(keyword=keyword, videos=videos)
        self.logger.info(f"Спарсили страницу результатов поиска по запросу '{keyword}'")
        yield search_page_item

    def parse_video(self, response):
        self.logger.info(f"Парсим страницу с видео {str(response.url).split('/')[4]}")
        # recommendations = self.get_recommendations(response)
        # self.logger.debug(f'Получил список {len(recommendations)} рекомендованных видео')

        href = response.css('strong.user_title>a::attr(href)').get()
        yield response.follow(href, callback=self.parse_channel)

        description = response.css('li.vdesc').get()
        description = str(description).replace('RU-clip', '<::domain_name::>')
        description = description.replace('ru-clip.net', '<::domain_name::>')

        if len(description) == len('<li class="vdesc">'):
            description = None

        video_item = VideoItem(
            video_id=str(response.url).split('/')[4],
            title=response.css('div#cldesc>h1::text').get(),
            description=description,
            upload_date=response.css('li.dateadded::text').get(),
            category=response.css('ul.desc-list>li>b>a::text').get(),
            views=response.css('span.views-count::text').get(),
            likes=response.css('span#l_num::text').get(),
            dislikes=response.css('span#disl_num::text').get(),
            channel_name=response.css('strong.user_title>a::text').get(),
            channel_id=response.css('strong.user_title>a::attr(href)').get().split('/')[2],
        )
        self.logger.info(f"Спарсили страницу с видео {str(response.url).split('/')[4]}")
        yield video_item

    def parse_channel(self, response):
        self.logger.info(f"Парсим инфо о канале {str(response.url).split('/')[-1]}")
        item_channel = ChannelItem(
            name=response.css('div.col-lg-9>h2').get(),
            channel_id=str(response.url).split('/')[-1],
            description=response.css('div.cdesc::text').get(),
            background_url=response.css('div.cover-holder>img.cb-live-background::attr(src)').get(),
            avatar_url=response.css('div.avatar>img.img-responsive::attr(src)').get()
        )
        self.logger.info(f"Спарсили инфо о канале {str(response.url).split('/')[-1]}")
        yield item_channel

    @staticmethod
    def get_comments(response) -> dict:
        comments = {}
        for index, comment in enumerate(response.css('ul#comments-ul>li')):
            comment_item = {
                'avatar_url': comment.css('div.comment-avatar>img::attr(data-original)').get(),
                'author_name': comment.css('div.comment-info>div.clearfix>a>strong::text').get(),
                'pub_date': comment.css('div.comment-info>div.clearfix>span::text').get(),
                'text': comment.css('div.comment-info>p::text').get(),
            }
            replies = comment.css('div.reply') or None
            if replies:
                replies_dict = {}
                for i, reply in enumerate(replies.css('ul>li.clearfix')):
                    _ = {
                        'avatar_url': reply.css('div.comment-avatar>img::attr(data-original)').get(),
                        'author_name': reply.css('div.comment-info>div.clearfix>a>strong::text').get(),
                        'pub_date': reply.css('div.comment-info>div.clearfix>span::text').get(),
                        'text': reply.css('div.clearfix>p::text').get(),
                    }
                    replies_dict.update({i: _})
                comment_item.update({'replies': replies_dict})

            comments.update({index: comment_item})
        return comments
