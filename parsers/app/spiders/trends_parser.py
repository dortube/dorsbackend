import locale

import scrapy

from parsers.app.items import TrendingPageItem
from parsers.utils import slugify, get_ruclip_video_item


# todo: парсить страницу видео

class RuClipTrendsParser(scrapy.Spider):
    name = "trends"
    allowed_domains = ["ru-clip.net"]

    def start_requests(self):
        locale.setlocale(locale.LC_ALL, ('RU', 'UTF8'))
        base_url = 'https://ru-clip.net/trending'
        countries = [
            "ru",
            "ar", "at", "au", "ba", "be", "bg", "bh", "bo", "br",
            "by", "ca", "ch", "cl", "co", "cr", "cy", "cz", "de", "dk",
            "do", "dz", "ec", "ee", "es", "fi", "fr", "gb", "ge", "gh",
            "gr", "gt", "hk", "hn", "hr", "hu", "id", "ie", "il", "in",
            "iq", "is", "it", "jm", "jo", "jp", "ke", "kr", "kw", "kz",
            "lb", "lk", "lt", "lu", "lv", "ly", "ma", "me", "mk", "mt",
            "mx", "my", "ng", "ni", "nl", "no", "np", "nz", "om", "pa",
            "pe", "ph", "pk", "pl", "pr", "pt", "py", "qa", "ro", "rs",
            "ae", "sa", "se", "sg", "si", "sk", "sn", "sv", "th", "tn",
            "tr", "tw", "tz", "ua", "ug", "us", "uy", "vn", "za", "zw",
        ]
        for country in countries:
            url = f'{base_url}/{country}'
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        """ Парсинг страницу трендов + отдельно видео, которые есть на странице. """
        country = str(response.url).split('/')[-1]
        self.logger.info(f'Начинаем парсинг трендов страны {country}')

        videos = []
        for video in response.css('div.videos-block'):
            try:
                upload_date = video.css('div.vid-info>div.clearfix>span::text').get()
            except IndexError:
                upload_date = None

            slug = slugify(video.css('strong.title>a::text').get())

            try:
                video = get_ruclip_video_item(video)
                video.update({'slug': slug, 'upload_date': upload_date})
                videos.append(video)
            except IndexError:
                continue
        search_page_item = TrendingPageItem(country=country, videos=videos)
        self.logger.info(f"Спарсили страницу трандов в стране'{country}'")
        yield search_page_item
