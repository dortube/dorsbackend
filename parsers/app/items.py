# -*- coding: utf-8 -*-
import scrapy


class SearchPageItem(scrapy.Item):
    """ Элемент страницы поиска"""
    keyword = scrapy.Field()
    videos = scrapy.Field()

    def __str__(self):
        return 'SearchPageItem'


class TrendingPageItem(scrapy.Item):
    videos = scrapy.Field()
    country = scrapy.Field()

    def __str__(self):
        return 'TrendingPageItem'


class LivesPageItem(scrapy.Item):
    videos = scrapy.Field()

    def __str__(self):
        return 'LivesPageItem'


class VideoItem(scrapy.Item):
    video_id = scrapy.Field()
    title = scrapy.Field()
    description = scrapy.Field()
    upload_date = scrapy.Field()
    category = scrapy.Field()
    views = scrapy.Field()
    likes = scrapy.Field()
    dislikes = scrapy.Field()
    channel_name = scrapy.Field()
    channel_id = scrapy.Field()

    # recommendations = scrapy.Field()  # пока решил не парсить рекомендации
    # comments = scrapy.Field()

    def __str__(self):
        return 'VideoItem'


class ChannelItem(scrapy.Item):
    name = scrapy.Field()
    channel_id = scrapy.Field()
    description = scrapy.Field()
    background_url = scrapy.Field()
    avatar_url = scrapy.Field()

    def __str__(self):
        return 'ChannelItem'
