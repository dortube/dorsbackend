#!/bin/bash

export SCRAPY_SETTINGS_MODULE=parsers.app.settings
export PYTHONPATH=/var/www/dorsbackend/:/var/www/dorsbackend/api:/var/www/dorsbackend/parsers/:/var/www/dorsbackend/db:/var/www/dorsbackend/parsers/app/spiders

cd /var/www/dorsbackend
. venv/bin/activate
cd parsers/app/spiders
scrapy crawl ruclip --logfile=logs/ruclip_logs.log
