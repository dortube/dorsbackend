import pytest
from falcon import testing
from peewee import SqliteDatabase, OperationalError

from api.app import create_app
from api.v2.parser import Parser
from db.models import Video, Channel, Category


@pytest.fixture(scope='session')
def db():
    models = (Video, Channel, Category)

    # use an in-memory SQLite for tests.
    test_db = SqliteDatabase(':memory:')

    test_db.bind(models, bind_refs=False, bind_backrefs=False)
    test_db.connect(reuse_if_open=True)
    try:
        test_db.create_tables(models, safe=False)
    except OperationalError:
        print('База уже создана. ', test_db.get_tables())
    yield test_db

    # teardown part
    test_db.drop_tables(models)

    # Close connection to db.
    test_db.close()


@pytest.fixture
def parser():
    parser = Parser()
    return parser


@pytest.fixture(scope='session')
def client():
    return testing.TestClient(create_app())


def test_video_resource_video_not_found(db, client):
    video_id = 'j_MAVx_dD2M_fake'
    result = client.simulate_get(f'/v2/video/{video_id}')
    assert result.json == {"error": f"Видео {video_id} нет в базе"}


def test_video_resource_new_video_added(db, client):
    video_id = 'j_MAVx_dD2M'
    result = client.simulate_get(f'/v2/video/{video_id}')
    # print(result.json)
    assert result.json != {"error": f"Видео {video_id} нет в базе"}


def test_process_video_item(db, parser):
    video_id = "7khUHbQFK9I"
    video_item = parser.fetch_video(video_id)
    video_obj = parser.process_video_item(video_item)
    assert isinstance(video_obj, Video)
    assert Video.get_or_none(Video.video_id == "7khUHbQFK9I")
