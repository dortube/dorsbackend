import json
import logging

import falcon
from falcon import Request, Response
from falcon_redis_cache.resource import CacheCompaitableResource
from playhouse.shortcuts import model_to_dict

from db.models import URLS, SearchPage, Video, Category, Channel, Trending, Lives

logger = logging.getLogger(__name__)


class SearchPageResource(CacheCompaitableResource):
    """ Ресурс получения SearchPage по id или по ключевой фразе, если указан параметр keyword """

    # @CacheProvider.from_cache
    def on_get(self, request: Request, response: Response):
        spage_id = request.get_param_as_int('id', default=None)
        keyword = request.get_param('keyword', default=None)
        if not any([keyword, spage_id]):
            raise falcon.HTTPMissingParam('(keyword, spage_id)')

        data = {}
        if spage_id:
            data = SearchPage.get_or_none(SearchPage.id == spage_id)
            if not data:
                raise falcon.HTTPNotFound(
                    description=f"SearchPage с таким id ({spage_id}) нет в базе",
                )
        elif keyword:
            data = SearchPage.get_or_none(SearchPage.slug == keyword)  # keyword здесь это = slug в базе
            if not data:
                if not data:
                    raise falcon.HTTPNotFound(
                        description=f"SearchPage по ключевой фразе {keyword} нет в базе",
                    )

        response.body = json.dumps(model_to_dict(data))
        response.status = falcon.HTTP_200


class SearchPageSlugOnlyResource(CacheCompaitableResource):
    """ Ресурс получения SearchPage, но выводится исключительно список slug'ов """

    # @CacheProvider.from_cache
    def on_get(self, request: Request, response: Response):
        items_per_page = request.get_param_as_int('size', default=10000)

        page = request.get_param_as_int('page', default=1)

        offset = items_per_page * page

        query = SearchPage.select(SearchPage.slug).offset(offset).limit(items_per_page).order_by(SearchPage.id.desc())
        if not query:
            raise falcon.HTTPNotFound(description="Не удалось получить список slug'ов всех SearchPage")

        total_pages = SearchPage.select().count() // items_per_page + 1
        data = {'pages_count': total_pages}

        if page < total_pages:
            next_page = page + 1
            data['next'] = f'/searchpage/slug?page={next_page}'

        data['res'] = [model_to_dict(x)['slug'] for x in query]

        response.body = json.dumps(data, default=str)
        response.status = falcon.HTTP_200


class SearchPageSimilarResource(CacheCompaitableResource):
    """
    Ресурс получения похожих запросов SearchPage.
    По факту возвращаются результаты, имеющие близкий к исходному запросу id
    (окружают его с определенным рейнджем)
    """

    # @CacheProvider.from_cache
    def on_get(self, request: Request, response: Response):
        spage = None

        keyword_slug = request.get_param('keyword', default=None)
        if keyword_slug:
            spage = SearchPage.get_or_none(SearchPage.slug == keyword_slug)
            if not spage:
                raise falcon.HTTPNotFound(
                    description=f"Не удалось получить SearchPage с названием {keyword_slug}",
                )

        slug_id = request.get_param('id', default=None)
        if slug_id:
            spage = SearchPage.get_or_none(SearchPage.id == slug_id)
            if not spage:
                raise falcon.HTTPNotFound(
                    description=f"Не удалось получить SearchPage с id {slug_id}",
                )

        if not any([keyword_slug, slug_id]):
            raise falcon.HTTPMissingParam(param_name="(keyword, id)")

        data = SearchPage.select(SearchPage.slug, SearchPage.keyword, SearchPage.id) \
            .filter(SearchPage.id > spage.id - 100, SearchPage.id <= spage.id + 100)

        response.body = json.dumps({'res': [model_to_dict(x) for x in data]}, default=str)
        response.status = falcon.HTTP_200


class VideoResource(CacheCompaitableResource):
    """ Ресурс возвращающий информацию о конкретном видео по параметру video_id """

    # @CacheProvider.from_cache
    def on_get(self, request: Request, response: Response, video_id):
        video = Video.get_or_none(Video.video_id == video_id)
        if not video:
            raise falcon.HTTPNotFound(description=f"Видео {video_id} нет в базе")

        response.body = json.dumps(model_to_dict(video), default=str)
        response.status = falcon.HTTP_200


class LastVideosResource(CacheCompaitableResource):
    def on_get(self, request: Request, response: Response):
        """
        Возвращает список самых свежих видео из базы (недавно выложенные на ютуб по upload_date)
        Можно задать количество видео параметром ?count=
        По умолчанию count=10
        """
        count = request.get_param_as_int('count', default=10)

        video = Video.select(
            Video.id, Video.video_id, Video.slug, Video.title
        ).order_by(Video.upload_date.desc())[:count + 1]

        if not video:
            raise falcon.HTTPNotFound(description="Не удалось получить список новых видео")

        response.body = json.dumps({'res': [model_to_dict(x) for x in video]}, default=str)
        response.status = falcon.HTTP_200


class StatsResource:
    def on_get(self, request: Request, response: Response):
        data = dict(videos_count=Video.select().count(),
                    searchpages_count=SearchPage.select().count(),
                    urls_count=URLS.select().count(),
                    categories_count=Category.select().count(),
                    channels_count=Channel.select().count()
                    )

        response.body = json.dumps(data, ensure_ascii=False)
        response.status = falcon.HTTP_200


class CategoriesResource(CacheCompaitableResource):
    """ Ресурс возвращающий Все категории из бд """

    # @CacheProvider.from_cache
    def on_get(self, request: Request, response: Response):
        categories = Category.select()
        if not categories:
            raise falcon.HTTPNotFound(description="Не удалось получить категории")

        response.body = json.dumps({'res': [model_to_dict(x) for x in categories]})
        response.status = falcon.HTTP_200


class CategoryResource(CacheCompaitableResource):
    """ Ресурс возвращающий видео относящиеся к заданной категории """

    # @CacheProvider.from_cache
    def on_get(self, request: Request, response: Response, category_id):
        _items_per_page = 20
        page = request.get_param_as_int('page', default=1)

        query = Video.select(
            Video.video_id,
            Video.slug,
            Video.views,
            Video.title,
            Video.description,
            Video.upload_date,
            Video.channel,
            Video.category,
        ).where(Video.category == category_id).order_by(Video.upload_date.desc())
        paginated_query = query.paginate(page, _items_per_page)

        total_pages = query.count() // _items_per_page + 1
        data = {}

        if page < total_pages:
            next_page = page + 1
            data['next'] = f'/category/{category_id}?page={next_page}'

        if not paginated_query:
            raise falcon.HTTPNotFound(
                description=f"Не удалось получить видео из заданной категории ({category_id})"
            )

        data['res'] = [model_to_dict(x) for x in paginated_query]
        response.body = json.dumps(data, default=str)
        response.status = falcon.HTTP_200


class VideosByChannelResource(CacheCompaitableResource):
    """ Ресурс возвращающий все видео относящиеся к заданной каналу """

    # @CacheProvider.from_cache
    def on_get(self, request: Request, response: Response, channel_id):
        _items_per_page = 20
        page = request.get_param_as_int('page', default=1)

        channel_obj = Channel.get_or_none(Channel.channel_id == channel_id)
        if not channel_obj:
            raise falcon.HTTPNotFound(description=f"Канал ({channel_id}) не найден в базе")

        query = Video.select(
            Video.video_id,
            Video.slug,
            Video.views,
            Video.title,
            Video.upload_date,
        ).where(Video.channel == channel_obj).order_by(Video.upload_date.desc())
        paginated_query = query.paginate(page, _items_per_page)

        total_pages = query.count() // _items_per_page + 1
        data = {}
        if page < total_pages:
            next_page = page + 1
            data['next'] = f'/category/{channel_id}?page={next_page}'

        if not paginated_query:
            raise falcon.HTTPNotFound(
                description=f"Не удалось получить видео из заданного канала ({channel_id})"
            )

        data['res'] = [model_to_dict(x) for x in paginated_query]
        data['channel_info'] = model_to_dict(channel_obj)

        response.body = json.dumps(data, default=str)
        response.status = falcon.HTTP_200


class TrendingVideosResource(CacheCompaitableResource):

    # @CacheProvider.from_cache
    def on_get(self, request: Request, response: Response):
        country = request.get_param('country', default='ru')
        videos = Trending.get_or_none(Trending.country == country)
        if not videos:
            response.body = json.dumps({"error": f"Не удалось получить тренды ({country})"}, ensure_ascii=False)
            response.status = falcon.HTTP_404
            return

        response.body = json.dumps({'res': videos.videos})
        response.status = falcon.HTTP_200


# todo: добавить лайвы для разных стран
class LivesVideosResource(CacheCompaitableResource):

    # @CacheProvider.from_cache
    def on_get(self, request: Request, response: Response):
        # country = request.get_param('country', default='ru')
        videos = Lives.select().first()
        if not videos:
            raise falcon.HTTPNotFound(description=f"Не удалось получить lives видео")

        response.body = json.dumps({'res': videos.videos})
        response.status = falcon.HTTP_200
