import logging

from falcon import Request, Response, HTTPUnauthorized

logger = logging.getLogger(__name__)

SECRET_TOKEN = '44MgwoSCotRJNJTljYWmm0tKIDv2YReb0ZkmekTqwU5bPVP1TpvOgNkVUlPyxDVnWauhgNaWjHD0Yy7q'
ALLOWED_HOSTS = [
    '127.0.0.1',
    '82.146.35.126',
    '46.161.39.138',
    '94.19.117.251'
]


class AuthMiddleware(object):

    def process_request(self, req: Request, resp: Response):
        token = req.get_header('Authorization')
        remote_ip = req.remote_addr
        # logger.debug(f'forwarded_host: {remote_ip}; subdomain: {req.subdomain}; '
        #             f'host: {req.host}; refferer: {req.referer}; remote_addr: {req.remote_addr}')

        if not self._token_is_valid(token, remote_ip):
            description = ('The provided auth token is not valid. '
                           'Please request a new token and try again.')

            logger.info(f'Auth Error! remote_ip: {remote_ip}, token: {token}')
            raise HTTPUnauthorized('Auth token required', description)

    def _token_is_valid(self, token, remote_ip):
        if remote_ip in ALLOWED_HOSTS:
            return True

        return token == SECRET_TOKEN
