import json
import logging

import falcon
from falcon import Request, Response
from playhouse.shortcuts import model_to_dict

from api.v2.parser import Parser
from db.models import Video

logger = logging.getLogger(__name__)


class VideoResource:
    """
        Ресурс возвращающий информацию о конкретном видео по параметру video_id.
        В случае, если видео не найдено, проверить можно ли его спарсить и добавить в базу.
    """

    def on_get(self, request: Request, response: Response, video_id):
        video = Video.get_or_none(Video.video_id == video_id)
        if not video:
            logging.debug('Видео нет в базе. Пробуем парсить')
            parser = Parser()
            video = parser.parse(video_id)
            logging.debug(f'Спарсили видео {video}')
            del parser
            if not video:
                response.body = json.dumps({"error": f"Видео {video_id} нет в базе"}, ensure_ascii=False)
                response.status = falcon.HTTP_404
                return

        response.body = json.dumps(model_to_dict(video), default=str)
        response.status = falcon.HTTP_200
