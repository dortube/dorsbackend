import logging
from dataclasses import dataclass

import requests
from slugify import slugify as Slugify

from db.models import Video, Channel, Category
from parsers.utils import slugify

logger = logging.getLogger(__name__)


@dataclass
class VideoItem:
    video_id: str
    title: str
    description: str
    upload_date: str
    category: str
    views: str
    likes: str
    dislikes: str
    channel_name: str
    channel_id: str


YoutubeCategory = {
    1: "Фильмы и анимация",
    2: "Транспорт",
    10: "Музыка",
    15: "Животные",
    17: "Спорт",
    18: "Короткометражные фильмы",
    19: "Путешествия",
    20: "Видеоигры",
    21: "Ведение видеоблога",
    22: "Люди и блоги",
    23: "Юмор",
    24: "Развлечения",
    25: "Новости и политика",
    26: "Хобби и стиль",
    27: "Образование",
    28: "Наука и техника",
    29: "Общество",
    30: "Кинозал",
    31: "Анимация и мультфильмы",
    32: "Приключения и боевики",
    33: "Классика",
    34: "Юмор",
    35: "Документальное кино",
    36: "Драмы",
    37: "Для всей семьи",
    38: "Зарубежное кино",
    39: "Ужасы",
    40: "Научная фантастика/фэнтези",
}


class Parser:

    def __init__(self):
        self._api_keys = [
            'AIzaSyBHK9-uu5JgfDrRJIh3fIfi5ir6PvfRTcA',  # project1
            'AIzaSyAt5Jtcl3Rx4h9CrvqFfTBwueH6nv8cbkw',  # project2
            'AIzaSyDLxwqoSVgtE68WMy4ECNC3fgiug_iJqLU',  # My Project 55080
            'AIzaSyDJqLrdC4aECarV6Zp_FITzkSR4eBDW_WY',  # My Project 37278
            'AIzaSyBDGITbqipIX6sZmNhZRiwLfoSn8hRaz2k',  # My Project 57608
            'AIzaSyDOgNgknNMPQiLq54fFMlj_gUC3o3fJOgg',
        ]
        self._index = 0
        self.key: str or None = None
        self.update_api_key()

    def update_api_key(self) -> bool:
        try:
            key = self._api_keys[self._index]
            self._index += 1
            self.key = key
            return True
        except IndexError:
            logging.warning('Закончились api youtube ключи')
            return False

    def parse(self, video_id: str) -> Video or None:
        item: VideoItem = self.fetch_video(video_id)
        if not item:
            return None
        video: Video = self.process_video_item(item)
        return video

    def fetch_video(self, video_id: str) -> VideoItem or None:
        url = f'https://www.googleapis.com/youtube/v3/videos?part=snippet%2Cstatistics&id={video_id}&key={self.key}'
        r = requests.get(url)
        data = r.json()

        if 'error' in data:
            if data['error']['errors'][0]['reason'] in ('keyInvalid', 'dailyLimitExceeded'):
                logging.debug(f'Ключ {self.key} закончился, обновляем.')
                if self.update_api_key():
                    return self.fetch_video(video_id)
                else:
                    return None

        if 'items' not in data or len(data['items']) == 0:
            logging.info('items not in data or video not found')
            return None

        video_id = data['items'][0]['id']
        upload_date = data['items'][0]['snippet']['publishedAt']
        channel_id = data['items'][0]['snippet']['channelId']
        title = data['items'][0]['snippet']['title']
        description = data['items'][0]['snippet']['description']
        channel_name = data['items'][0]['snippet']['channelTitle']
        likes = data['items'][0]['statistics']['likeCount']
        dislikes = data['items'][0]['statistics']['dislikeCount']
        views = data['items'][0]['statistics']['viewCount']
        category_id = data['items'][0]['snippet']['categoryId']

        # приводим к общему виду
        upload_date = str(upload_date).split('T')[0]
        category = YoutubeCategory[int(category_id)]

        item = VideoItem(
            video_id=video_id, title=title, description=description, upload_date=upload_date, category=category,
            views=views, likes=likes, dislikes=dislikes, channel_name=channel_name, channel_id=channel_id
        )
        return item

    @staticmethod
    def process_video_item(item: VideoItem) -> Video:
        # ищем и если отсутствует - то создаем новый обьект канала
        # к сожалению, на данном этапе о канале известно только его имя и id,
        # поэтому остальную инфу придется парсить отдельно.
        channel_obj = Channel.get_or_none(channel_id=item.channel_id)
        if not channel_obj:
            channel_obj = Channel(
                name=item.channel_name,
                channel_id=item.channel_id,
                description=None,
                background_url=None,
                avatar_url=None,
            )
            channel_obj.save()
            logger.debug(f"Канал {channel_obj.channel_id} успешно добавлен в базу с дефолтными полями")

        # формируем словарь с лайками
        likes = {'likes': item.likes, 'dislikes': item.dislikes}

        category_obj = None
        if item.category:
            category_obj = Category.get_or_none(name=item.category)
            if not category_obj:
                category_obj = Category.create(name=item.category, slug=Slugify(item.category))

        slug = slugify(item.title)

        # logger.debug(f"Подготовлены поля к сохранению: {item.keys()}")

        views = f'Просмотров {item.views}'

        # наконец создаем новый обьект
        video, _ = Video.get_or_create(
            video_id=item.video_id,
            slug=slug,
            views=views,
            title=item.title,
            description=item.description,
            likes=likes,
            upload_date=item.upload_date,
            channel=channel_obj,
            category=category_obj
        )
        logger.debug(f"Видео {item.video_id} успешно добавлено в базу")
        return video
