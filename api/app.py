import logging
import os

import falcon

import api.resources as resources
from api.v2 import resources as resources_v2
from middleware import AuthMiddleware


def create_app() -> falcon.API:
    project_root = os.path.abspath(os.curdir)
    log_path = os.path.join(project_root, 'logs/api_logs.log')

    logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                        level=logging.INFO, filename=log_path)

    # init falcon app
    app = falcon.API(
        middleware=[
            AuthMiddleware(),
            # RedisCacheMiddleware(redis_host='127.0.0.1', redis_port='6379', redis_db=0)
        ]
    )

    # add routes
    app.add_route('/searchpage/', resources.SearchPageResource())
    app.add_route('/searchpage/slug', resources.SearchPageSlugOnlyResource())
    app.add_route('/searchpage/similar', resources.SearchPageSimilarResource())

    app.add_route('/video/{video_id}', resources.VideoResource())
    app.add_route('/v2/video/{video_id}', resources_v2.VideoResource())

    app.add_route('/channel-videos/{channel_id}', resources.VideosByChannelResource())
    app.add_route('/last-videos', resources.LastVideosResource())

    app.add_route('/trending', resources.TrendingVideosResource())
    app.add_route('/lives', resources.LivesVideosResource())

    app.add_route('/stats/', resources.StatsResource())

    app.add_route('/categories', resources.CategoriesResource())
    app.add_route('/category/{category_id}', resources.CategoryResource())
    return app
