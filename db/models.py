import datetime

from playhouse.postgres_ext import (
    PostgresqlDatabase, Model, CharField, BooleanField, DateTimeField,
    BinaryJSONField, InternalError, DateField, ForeignKeyField
)

user = 'postgres'
password = 'Trans_150690'
db_name = 'ruclip_parsed'
srv_ip = '46.161.39.138'
port = 5432

db = PostgresqlDatabase(db_name, user=user, password=password, host=srv_ip, port=port, autorollback=True)


class URLS(Model):
    """ Модель таблицы со списком урлов, которые необходимо пройти или которые уже пройдены """
    url = CharField(max_length=500, unique=True, null=False)
    is_crawled = BooleanField(null=False, default=False, verbose_name='URL пройден ботом')
    crawl_date = DateTimeField(null=True, verbose_name='Дата прохождения ботом')

    class Meta:
        database = db

    def __str__(self):
        return str(self.url)


class Category(Model):
    """ Модель категории видео (Спорт, Фильмы и т.д.) """
    name = CharField(max_length=300, unique=True, index=True, null=False)
    slug = CharField(max_length=255, index=True, null=False)

    class Meta:
        database = db


class Channel(Model):
    name = CharField(max_length=200, null=False)
    channel_id = CharField(max_length=300, index=True, unique=True, null=False)
    description = CharField(max_length=1500, null=True)
    background_url = CharField(max_length=600, null=True)
    avatar_url = CharField(max_length=600, null=True)

    class Meta:
        database = db


class Video(Model):
    video_id = CharField(max_length=100, unique=True, null=False)
    slug = CharField(max_length=255, index=True, null=False)
    views = CharField(max_length=50)
    title = CharField(max_length=250, null=False)
    description = CharField(max_length=5000, null=True)
    likes = BinaryJSONField()
    upload_date = DateField(index=True)
    channel = ForeignKeyField(Channel, backref='videos', null=False)
    category = ForeignKeyField(Category, backref='videos', null=True)

    class Meta:
        database = db


class SearchPage(Model):
    keyword = CharField(max_length=300, index=True, unique=True, null=False)
    slug = CharField(max_length=255, index=True, unique=True, null=False)
    videos = BinaryJSONField()

    class Meta:
        database = db


class Trending(Model):
    country = CharField(max_length=150, null=False, index=True, unique=True)
    date_update = DateTimeField(default=datetime.datetime.now)
    videos = BinaryJSONField()

    class Meta:
        database = db


class Lives(Model):
    last_update = DateTimeField(default=datetime.datetime.now)
    videos = BinaryJSONField()

    class Meta:
        database = db


def create_tables():
    try:
        Category.create_table()
    except InternalError as px:
        print(str(px))

    try:
        Channel.create_table()
    except InternalError as px:
        print(str(px))

    try:
        Video.create_table()
    except InternalError as px:
        print(str(px))

    try:
        SearchPage.create_table()
    except InternalError as px:
        print(str(px))

    try:
        URLS.create_table()
    except InternalError as px:
        print(str(px))


if __name__ == '__main__':
    db.connect()
    # create_tables()
    Lives.create_table()
